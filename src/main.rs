use std::fs;
use toml::{map::Map, Value}; // 0.5.9

fn to_toml(stripping:bool) -> Value { // package section
    let mut toml = Map::new();

    // read block
    //let name = read("package name".to_string());

    let name = "test".to_string();
    let ver = "0.3".to_string(); 
    let des = "fsdfgsakfbdf".to_string();
    let mut package = Map::new();
    package.insert("name".into(), Value::String(name));
    package.insert("ver".into(), Value::String(ver));
    package.insert("des".into(), Value::String(des));
    package.insert("stripping".into(), Value::Boolean(stripping));
    toml.insert("package".to_string(), Value::Table(package));


    //let mut map = Map::new();
    //map.insert("package".into(), Value::Table(servers));
    Value::Table(toml)
}

fn read(text: String) -> String {
   let mut line = String::new();
   println!("{}",text);
   let _ = std::io::stdin().read_line(&mut line).unwrap();
   line
}
fn main() {
    let toml_string = toml::to_string(&to_toml(true)).expect("Could not encode TOML value");
    println!("{}", toml_string);

    fs::write("servers.toml", toml_string).expect("Could not write to file!");
}
